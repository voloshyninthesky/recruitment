class Integer < Numeric
  def choose(k)
    n = self.to_i
    answ = 1
    for i in 1..k
      answ = answ * (n - i + 1) / i
    end
    answ
  end
end

class Array
  def square
    self.inject([]) {|a, b| a << b**2 }
  end

  def average
    self.inject{ |sum, i| sum + i} / self.size
  end

  def even
    self.reject{|i| i % 2 != 0}
  end
end
