require 'rails_helper'

RSpec.describe ExampleUser do
  describe 'full_name' do
    it 'returns full_name' do
      expect(ExampleUser.new.full_name).to eq                                'Anonymous'
      expect(ExampleUser.new(name: 'Kurt', second_name: 'Donald', surname: 'Cobain').full_name).to eq  'Kurt Donald Cobain'
      expect(ExampleUser.new(name: 'Kurt', surname: 'Cobain').full_name).to eq  'Kurt Cobain'
      expect(ExampleUser.new(name: 'Kurdt').full_name).to eq  'Anonymous'
    end
  end
end
