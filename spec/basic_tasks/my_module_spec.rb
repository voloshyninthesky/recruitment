require 'rails_helper'

RSpec.describe MyModule do
  let(:dummy_class) { Class.new { include MyModule } }

  describe '#instance_method' do
    context 'when is called on instance obj' do
      subject { dummy_class.new.instance_bar_method }
      it { is_expected.to eq 'My instance method' }
    end

    context 'when is called on class obj' do
      subject { dummy_class.instance_bar_method }

      it 'raise an error' do
        expect { subject }.to raise_error(NoMethodError)
      end
    end
  end


  describe '.class_method' do
    context 'when is called on instance obj' do
      subject { dummy_class.new.class_foo_method }

      it 'raise an error' do
        expect { subject }.to raise_error(NoMethodError)
      end
    end

    context 'when is called on class obj' do
      subject { dummy_class.class_foo_method }
      it { is_expected.to eq 'My class method' }
    end
  end
end
