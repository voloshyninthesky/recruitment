require 'rails_helper'

RSpec.describe ExampleFunctions do

  describe '#likes' do
    it 'returns array of users who likes sth' do
      expect(ExampleFunctions.likes([])).to eq                                'no one likes this'
      expect(ExampleFunctions.likes(['Peter'])).to eq                         'Peter likes this'
      expect(ExampleFunctions.likes(['Jacob', 'Alex'])).to eq                 'Jacob and Alex like this'
      expect(ExampleFunctions.likes(['Jacob', 'Alex', 'Mark'])).to eq         'Jacob, Alex and Mark like this'
      expect(ExampleFunctions.likes(['Jacob', 'Alex', 'Mark', 'Max'])).to eq  'Jacob, Alex and 2 others like this'
    end
  end

  describe '#find_short' do
    it 'returns the length of the shortest word' do
      expect(ExampleFunctions.find_short('bitcoin take over the world maybe who knows perhaps')).to eq 3
      expect(ExampleFunctions.find_short('turns out random test cases are easier than writing out basic ones')).to eq 3
      expect(ExampleFunctions.find_short('i want to travel the world writing code one day')).to eq  1
      expect(ExampleFunctions.find_short('travel')).to eq  6
    end
  end

end
