# Zadanie rekrutacyjne

Zadanie polega na wykonaniu issues z tego repozytorium. Osoby aplikujące na stanowisko Juniora powinny wykonać
wszystkie zadania z priorytetem `minor` (choć oczywiście do pozostałych również można podejść). Z kolei osoby aplikujące 
na stanowisko Mida `minor` oraz `major`. Wszystkie zadania powinny być wykonane wewnątrz aplikacji, 
pomimo że nie wszystkie jej bezpośrednio dotyczą.

## Instalacja


### Baza danych

Do wykonania zadania potrzebny jest MySQL. Aplikacja była testowana na MySQL w wersji 5.7.19.

Kompletną bazę danych z dodanymi rekordami można pobrać tutaj:

https://drive.google.com/open?id=1Q85liYYmU2HyLczMmPwoZwD92ECz_ReL

SQL tworzy bazy danych `rekrutacja-dev` i `rekrutacja-test`. Bazę ze środowiska development uzupełnia dodatkowo danymi.
Plik po rozpakowaniu zajmuje ok. 230 MB. Taka ilość danych jest konieczna, aby wykonać zadanie optymalizacyjne.

### Aplikacja

Aplikacja była testowana na systemie Mac OS z Rubym w wersji 2.3.3.

Aplikację należy sklonować z tego repozytorium, a następnie utworzyć w katalogu `config` dwa pliki: 
`secrets.yml` i `database.yml`. W katalogu tym znajdują się już dwa pliki `secrets.yml-dev` i `database.yml-dev`,
więc wystarczy je skopiować ze zmienionym rozszerzeniem. Zalecamy nie zmieniać nazw baz danych i skorzystać
z tych, które znajdują się w pliku z rozszerzeniem `.yml-dev`. 

Pozostaje jedynie zainstalować gemy i po uruchomieniu serwera aplikacja powinna być już dostępna w przeglądarce.

Wszyscy stworzeni w bazie użytkownicy mają ustawione hasło `password`.

Przykładowe dane do logowania:

```
Email:    yolanda@dach.net
Password: password
```

### Jak rozwiązywać zadania?

Zazwyczaj nie wystarczy, żeby kod tylko działał. Należy zwracać także uwagę, 
żeby był jak najbardziej czytelny, zwięzły i logiczny.

### Publikacja własnych zadań

Po wykonaniu zadań według issues, należy wrzucić aplikację do własnego prywatnego repozytorium, a następnie dodać
do niego użytkownika `johnnybros`. W README swojego repozytorium pomocne może okazać
się umieszczenie objaśnień do niektórych zadań.

### Problemy

W razie gdyby pojawiły się problemy lub pytania prosimy o kontakt: `jan@johnnybros.com`.


