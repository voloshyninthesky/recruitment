=begin
Parallel.each(1..20000, in_processes: 8) do |i|
  puts "user: #{i}"
  u = User.create(email: Faker::Internet.email, password: 'password', roles: [:user])
end
=end

created_ids = User.all.pluck(:id)
created_ids.each do |i|
  r = rand(50)
  without_current = created_ids - [i]
  Parallel.each(1..r, in_processes: 8) do |j|
    puts "follow: #{j}"
    UserFollowing.create(user_id: i, following_id: without_current.sample)
  end
end

ActiveRecord::Base.connection.execute('DELETE u1 FROM user_followings u1, user_followings u2 WHERE u1.id > u2.id AND u1.user_id = u2.user_id AND u1.following_id = u2.following_id')

=begin

created_ids.each do |i|
  r = rand(500)
  Parallel.each(1..r, in_processes: 8) do |j|
    puts "post: #{j}"
    p = Post.create(author_id: i, content: Faker::Lorem.paragraph)
    r2 = rand(4)
    for k in 0..r2
      puts "comment: #{k}"
      p.comments.create(comment: Faker::Lorem.paragraph, user_id: created_ids.sample)
    end
  end

  r = rand(20)
  without_current = created_ids - [i]
  Parallel.each(1..r, in_processes: 8) do |j|
    puts "follow: #{j}"
    UserFollowing.create(user_id: i, following_id: without_current.sample)
  end
end
=end
