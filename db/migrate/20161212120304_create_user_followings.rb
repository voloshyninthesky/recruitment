class CreateUserFollowings < ActiveRecord::Migration
  def change
    create_table :user_followings do |t|
      t.integer :user_id
      t.integer :following_id

      t.timestamps null: false
    end
    add_index :user_followings, :user_id
    add_index :user_followings, :following_id
    add_index :user_followings, [:user_id, :following_id], unique: true
  end
end
