class AddRolesToUser < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.text :name
      t.timestamps null: false
    end

    add_column :users, :roles, :integer
  end
end
