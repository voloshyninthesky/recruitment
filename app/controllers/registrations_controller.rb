class RegistrationsController < Devise::RegistrationsController
  before_action :build_resource_with_additional_params, only: [:create]

  def create
    if resource.save
      register(resource, resource_name)
    else
      register_error(resource)
    end
  end

  private

  def build_resource_with_additional_params
    build_resource(sign_up_params.merge(roles: [:user]))
  end

  def register(resource, resource_name)
    respond_to do |format|
      format.json {
        sign_up(resource_name, resource)
        render json: {:location => root_path}, status: 200
      }
      format.html {
        yield resource if block_given?
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_flashing_format?
          sign_up(resource_name, resource)
          respond_with resource, :location => root_path
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
          expire_data_after_sign_in!
          respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        end
      }
    end
  end

  def register_error(resource)
    respond_to do |format|
      format.json { render json: {:errors => resource.errors}, status: 422 }
      format.html {
        clean_up_passwords resource
        respond_with resource
      }
    end
  end
end
