class UsersController < ApplicationController

  def index
    @users = User.all.page(params[:page]).per(100)
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.page(params[:page]).per(10)
    if current_user
      @user.guests.delete(current_user) if @user.guests.include?(current_user)
      @user.guests << current_user
    end
  end

  def dashboard
    authorize! :read_dashboard, current_or_guest_user
    @posts = current_user.dashboard_posts.page(params[:page]).per(10)
  end

end
