class PostsController < ApplicationController
  def create
    authorize! :create_post, current_or_guest_user
    post = Post.new(params[:post].permit(:content).merge({author_id: current_user.id}))
    status = post.save
    render json: { post: status ? render_to_string(partial: 'posts/post', locals: { post: post }) : '',
                   form: render_to_string(partial: 'posts/form', locals: { post: status ? Post.new : post}) },
           status: status ? :ok : :unprocessable_entity
  end
end