class CommentsController < ApplicationController
  def create
    authorize! :create, Comment
    commentable = Post.find(params[:comment][:commentable_id])
    comment = Comment.new(params[:comment].permit(:comment, :commentable_id, :commentable_type).merge({user_id: current_user.id}))
    status = comment.save
    render json: { comment: status ? render_to_string(partial: 'comments/comment', locals: { comment: comment }) : '',
                   form: render_to_string(partial: 'comments/form', locals: { post: commentable,
                                                                              comment: status ? Comment.new(commentable: commentable) : comment }) },
           status: status ? :ok : :unprocessable_entity
  end

  def destroy
    comment = Comment.find(params[:id])
    authorize! :destroy, comment
    status = comment.destroy
    render json: {}, status: status ? :ok : :unprocessable_entity
  end
end
