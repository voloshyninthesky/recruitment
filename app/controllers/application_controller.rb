class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  helper_method :current_or_guest_user
  helper_method :guest_user

  rescue_from CanCan::AccessDenied do |exception|
    if request.xhr?
      render nothing: true, status: :unauthorized
    else
      redirect_to new_user_session_path
    end
  end

  def current_ability
    @current_ability ||= Ability.new(current_or_guest_user)
  end

  def create_guest_user
    u = User.create(email: "guest_#{SecureRandom.uuid[0..20]}@cjgroup.pl", roles: [:guest])
    session[:guest_user_id] = u.id
    puts "create session[:guest_user_id]: #{session[:guest_user_id]}"
    puts "u.id: #{u.id}"
    u
  end

  def current_or_guest_user
    if current_user
      puts "current user"
      if session[:guest_user_id] && session[:guest_user_id] != current_user.id
        logging_in
        guest_user(with_retry = false).try(:destroy)
        session[:guest_user_id] = nil
      end
      current_user
    else
      puts "guest"
      guest_user
    end
  end

  private

  def logging_in
    # For example:
    # guest_comments = guest_user.comments.all
    # guest_comments.each do |comment|
    # comment.user_id = current_user.id
    # comment.save!
    # end
  end

  def guest_user(with_retry = true)
    puts "session[:guest_user_id]: #{session[:guest_user_id]}"
    @cached_guest_user ||= User.find(session[:guest_user_id] ||= create_guest_user.id)
  rescue ActiveRecord::RecordNotFound # if session[:guest_user_id] invalid
    session[:guest_user_id] = nil
    guest_user if with_retry
  end
end
