class Ability
  include CanCan::Ability

  def initialize(user)
    user_abilities(user) if user.roles?(:user)
  end

  private

  def user_abilities(user)
    can :read_dashboard, User do |u|
      u.id == user.id
    end

    can :create, Comment
    can :destroy, Comment do |c|
      c.user_id == user.id
    end

    can :create_post, User do |author|
      author.id == user.id
    end
  end
end
