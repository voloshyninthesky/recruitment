class Role < ActiveRecord::Base
  def self.available_roles
    roles = []
    if ActiveRecord::Base.connection.table_exists?("roles")
      roles = Role.all.collect(&:name)
      [:guest, :user].each do |r|
        roles.unshift(Role.create(name: r).name) unless roles.include?(r.to_s)
      end
      roles = roles.map(&:to_sym)
    end

    roles
  end
end
