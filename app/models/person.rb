class Person < ActiveRecord::Base
  belongs_to :parent, class_name: "Person"

  has_many :children, class_name: "Person", foreign_key: :parent_id
  scope :main_items, -> { where(parent: nil) }


  def grandchildren()
    self.children.map(&:children).flatten
  end
end
