class Post < ActiveRecord::Base
  belongs_to :author, class_name: 'User'
  validates :content, presence: true

  default_scope -> { order(created_at: :desc) }

  acts_as_commentable

  #censorable :content, words: %w(tempora ullam ipsum), replace_with: '***'

  scope :dashboard_for_user, ->(user) { where(author_id: UserFollowing.where(user_id: user.id).pluck(:following_id).push(user.id)) }
end
