class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable
  has_many :posts, foreign_key: :author_id
  has_many :active_relationship, class_name: "UserFollowing",
                                 foreign_key: "user_id",
                                 dependent: :destroy

  has_many :followed, through: :active_relationship, source: :following

  has_many :passive_relationships, class_name: "UserFollowing",
                                   foreign_key: "following_id",
                                   dependent: :destroy

  has_many :followers, through: :passive_relationships, source: :user
  has_many :user_guests
  has_many :guests, through: :user_guests, source: :guest

  validates :email, presence: true, uniqueness: true

  bitmask :roles, :as => Role.available_roles

  def dashboard_posts
    Post.dashboard_for_user(self)
  end

  def follow?(user_id)
    #zwraca czy obserwujemy usera o id = user_id
    followed.include?(User.find_by(id: user_id))
  end

  def follow(user_id)
    followed << user_id
  end

  def unfollow(user_id)
    followed.delete(user_id)
  end

  def follow_suggestions
    #zwraca propozycje kogo mozemy obserwować czyli osoby obserwowane przez osoby, które obserwujemy (5 losowo wybranych)
    user_ids = UserFollowing.where(user_id: UserFollowing.where(user_id: self.id).map { |f| f.following_id })
    user_ids = user_ids.where.not(following_id: UserFollowing.where(user_id: self.id).map { |f| f.following_id })
    user_ids = user_ids.where.not(following_id: self.id)
    user_ids = user_ids.map{|f| f.following_id}
    users = User.where(id: user_ids).sample(5)
    users
  end
end
