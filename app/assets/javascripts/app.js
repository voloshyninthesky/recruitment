var App = {
  addEventsDelegation: function () {

    $('#infinite-wrapper').infinitePages({
      buffer: 200
    });

    $(document).on('click', '.destroy-comment', function () {
      var $that = $(this);
      $.ajax({
        url: $that.data('href'),
        success: function () {
          $that.closest('.comment').remove();
        },
        method: 'DELETE'
      });
    });

    $(document).on('ajax:success', '#new_post', function (xhr, data) {
      $('#posts').prepend(data.post);
      $(this).replaceWith(data.form);
    });

    $(document).on('ajax:error', '#new_post', function (xhr, status) {
      $(this).replaceWith(status.responseJSON.form);
    });

    $(document).on('ajax:success', '#new_comment', function (xhr, data) {
      $(this).closest('.comments-wrapper').find('.comments').append(data.comment);
      $(this).replaceWith(data.form);
    });

    $(document).on('ajax:error', '#new_comment', function (xhr, status) {
      $(this).replaceWith(status.responseJSON.form);
    });
  },

  init: function () {
    this.addEventsDelegation();
  }
};
