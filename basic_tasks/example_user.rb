class ExampleUser
  mattr_accessor :name
  mattr_accessor :second_name
  mattr_accessor :surname

  def initialize(args=nil)
    args = {} if args.nil?
    @@name = args[:name]
    @@second_name = args[:second_name]
    @@surname = args[:surname]
  end

  def full_name
    return 'Anonymous' if @@surname.nil? or @@name.nil?
    second_name = @@second_name.nil? ? " " : " " + @@second_name + " "
    return @@name + second_name + @@surname
  end
end
