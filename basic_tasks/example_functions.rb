module ExampleFunctions
  extend self

  def likes(array_of_names)
    len = array_of_names.length
    if len == 0
      'no one likes this'
    elsif len == 1
      array_of_names[0] + ' likes this'
    elsif len < 4
      resp = array_of_names[0..-2].join(', ') + ' and ' + array_of_names[-1];
      resp = resp + ' like this'
    else
      array_of_names[0..1].join(', ') + ' and ' + (len-2).to_s + ' others like this'
    end
  end

  def find_short(str)
    words = str.split(/\W+/)
    words.sort_by!(&:length)
    words[0].length
  end
end
